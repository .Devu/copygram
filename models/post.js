const mongoose = require("mongoose")

let Schema = mongoose.Schema({
    text: String,
    id: String,
    user: {type:String, indexed: true}
})

module.exports = mongoose.model("Post", Schema)