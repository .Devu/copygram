const mongoose = require("mongoose")

let Schema = mongoose.Schema({
    name: { type: "String", unique: true},
    text: String
})

module.exports = mongoose.model("Data", Schema)