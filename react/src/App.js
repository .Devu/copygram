import React from 'react';
import './App.css';
import axios from 'axios'
// import { Grid, Placeholder } from 'semantic-ui-react'
// import Posts from './components/Posts';
import NaviBar from './components/NaviBar';
// import SubMenu from './components/SubMenu';
// import UserInfo from './components/UserInfo';
import {Switch,Route,Redirect} from 'react-router-dom';
import Login from './components/Login';
import User from './components/User';
import Post from './components/Post';
import NewPost from './components/NewPost';

class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      users: [],
      posts: '',
      comments: [],
      date: [],
      time: '',
      isLogged: false,
      token: '',
      user: '',
    }
  }

  componentDidMount() {
    let url = "http://localhost:3003/"

    axios.get(url + "users").then((res) => {
    //   console.log(res.data)
      this.setState({ users: JSON.stringify(res.data)})
    })

    axios.get(url + "posts").then((res) => {
      // console.log(res.data)
      this.setState({ posts: res.data})
    })

    axios.get(url + "comments").then((res) => {
      // console.log(res.data)
      this.setState({ comments: res.data})
    })

		if(sessionStorage.getItem("state")) {
			let state = JSON.parse(sessionStorage.getItem("state"));
			this.setState(state, () => {
				if(this.state.isLogged) {
				}
			})
		}

  }

  saveToStorage = () => {
		sessionStorage.setItem("state",JSON.stringify(this.state));
	}
	
	clearState = () => {
		this.setState({
			isLogged:false,
			list:[],
			token:""
		}, () => {
			this.saveToStorage();
		})
	}

  register = (user) => {
		let request = {
			method:"POST",
			mode:"cors",
			headers:{"Content-type":"application/json"},
			body:JSON.stringify(user)
		}
		fetch("http://localhost:3003/register",request).then(response => {
			if(response.ok) {
				alert("Register success!");
			} else {
				console.log("Server responded with a status:",response.status);
			}
		}).catch(error => {
			console.log("Server responded with an error. Reason:",error);
		});
	}

	login = (user) => {
		let request = {
			method:"POST",
			mode:"cors",
			headers:{"Content-type":"application/json"},
			body:JSON.stringify(user)
		}
		fetch("http://localhost:3003/login",request).then(response => {
			if(response.ok) {
				response.json().then(data => {
					this.setState({
						isLogged:true,
						token:data.token,
            			user: user.username
					},() => {
						this.saveToStorage();
					})
				}).catch(error => {
					console.log("Failed to parse JSON. Error:",error);
				})
			} else {
				console.log("Server responded with a status:",response.status);
			}
		}).catch(error => {
			console.log("Server responded with an error. Reason:",error);
		});
	}	
	
	logout = () => {
		let request = {
			method:"POST",
			mode:"cors",
			headers:{"Content-type":"application/json",
					token:this.state.token}
		}
		fetch("http://localhost:3003/logout",request).then(response => {
			if(response.ok) {
				this.clearState();
			} else {
				console.log("Server responded with status:",response.status);
				this.clearState();
			}
		}).catch(error => {
			console.log("Server responded with an error:",error);
		});
	}

  render() {

    return (
      <div style={{ maxWidth: '50rem', margin: 'auto', textAlign: 'center', paddingTop: '1rem' }}>
        <NaviBar isLogged={this.state.isLogged} logout={this.logout} ></NaviBar>

        <Switch>
          <Route exact path='/' render={() => this.state.isLogged ? 
          (<Redirect to='/user'/>) : (<Login register={this.register} login={this.login}></Login>) 
          } />

          <Route path='/user' render={() => this.state.isLogged ? 
          (<User user={this.state.user} token={this.state.token} />) : (<Redirect to='/' />)
        } />

		<Route path='/newpost' render={() => this.state.isLogged ? 
          (<NewPost user={this.state.user} token={this.state.token} />) : (<Redirect to='/' />)
        } />


		<Route path='/posts/:id' component={Post} />
		
        </Switch>
      </div>
    );
  }
}

export default App;