import React, { Component } from 'react'
import { Menu, Icon } from 'semantic-ui-react'
import {Link} from 'react-router-dom';

export class NaviBar extends Component {
    render() {
        if (this.props.isLogged) {
            return (
                <div>
                    <Menu fluid widths={5}> 
                        <Menu.Item>
                            <Link to='/'>
                                <Icon name='home'></Icon>
                            </Link>
                        </Menu.Item>      
                        <Menu.Item>
                            <Link to='/newpost'>
                                <Icon name='photo'></Icon>
                            </Link>
                        </Menu.Item>
                        <Menu.Item >
                            <Icon name='search'></Icon>
                        </Menu.Item>
                        <Menu.Item >
                            <Icon name="setting"></Icon>
                        </Menu.Item> 
                        <Menu.Item>
                            <Link to='/' onClick={() => this.props.logout()}>
                                <Icon name="log out"></Icon>
                            </Link>
                        </Menu.Item> 
                    </Menu>
                </div>
            )
        } else {
            return (
                <React.Fragment>
                    
                </React.Fragment>
            )
        }
    }
}

export default NaviBar
