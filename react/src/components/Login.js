import React, { Component } from 'react'
import {Form, Button, Placeholder} from 'semantic-ui-react';
import {Link} from 'react-router-dom';

export class Login extends Component {

    constructor(props) {
        super(props)
        this.state= {
            username: '',
            password: '',
            login: true
        }
    }

	onChange = (event) => {
		let state = {};
		state[event.target.name] = event.target.value;
		this.setState(state);
	}
	
	onSubmit = (event) => {
		event.preventDefault();
		let user = {
			username:this.state.username,
			password:this.state.password
		}
		if(event.target.name === "register") {
			this.props.register(user);
		} else {
			this.props.login(user);
		}
	}

    register = () => {
        this.setState(prevState => ({ 
            login: !prevState.login
        }))
    }

    render() {
        let mainStyle = {
            paddingTop: '10%', 
            width: '30rem', 
            margin: 'auto'
        }
        let subStyle ={
            border: '1px solid rgba(var(--b6a,219,219,219),1)',
            padding: '2rem' 
        }
        if (this.state.login) {
        return (
            <div style={mainStyle}>
                <h2>Login</h2>
               <div style={subStyle}>
                    <Placeholder style={{ height: '10rem' }}></Placeholder>
                    <br />
                        <Form>
                            <Form.Field>
                                <input type="text"	
                                        name="username"
                                        onChange={this.onChange}
                                        value={this.state.username}
                                        placeholder='username'/>
                            </Form.Field>
                            <Form.Field>
                                <input type="password"	
                                        name="password"
                                        onChange={this.onChange}
                                        value={this.state.password}
                                        placeholder='password'/>
                            </Form.Field>
                            <Button fluid onClick={this.onSubmit} name="login" >Login</Button>
                        </Form>
                </div>
                <br />
                <div style={subStyle}>
                    <h5>Dont have an account?  <Link to='/' onClick={() => this.register()}>Sign up</Link></h5>
                </div>
            </div>
        )
        } else {
            return (
                <div style={mainStyle}>
                <h2>Register</h2>
               <div style={{ border: '1px solid rgba(var(--b6a,219,219,219),1)', padding: '2rem' }}>
                    <Placeholder style={{ height: '10rem' }}></Placeholder>
                    <br />
                        <Form>
                            <Form.Field>
                                <input type="text" name="username" onChange={this.onChange} value={this.state.username} placeholder='username'/>
                            </Form.Field>
                            <Form.Field>
                                <input type="password" name="password" onChange={this.onChange}value={this.state.password} placeholder='password'/>
                            </Form.Field>
                            <Button fluid onClick={this.onSubmit} name="register">Register</Button>
                        </Form>
                </div>
                <br />
                <div style={subStyle}>
                    <h5>Have an account? <Link to='/' onClick={() => this.register()}>Login</Link></h5>
                </div>
            </div>
            )
        }
    }
}

export default Login
