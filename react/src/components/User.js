import React, { Component } from 'react'
import { Grid, Placeholder } from 'semantic-ui-react'
import Posts from './Posts'
import SubMenu from './SubMenu'
import UserInfo from './UserInfo'
// import axios from 'axios'

export class User extends Component {

    constructor(props) {
        super(props)
        this.state = {
            posts: '',
        }
    }

    componentDidMount() {
        let url = "http://localhost:3003/api/post"

        let request = {
			method:"GET",
			mode:"cors",
			headers:{"Content-type":"application/json", token: this.props.token}
        }
        
        fetch(url, request).then(response => {
            if(response.ok) {
                response.json().then(data => {
                    // console.log(data)
                    this.setState({
                        posts: data
                    })
                })
            }
        })
        
    }

    render() {
        return (
            <div>
                <Grid celled>
                <br />
                <Grid.Row centered columns="2">
                    <Grid.Column width="6">
                    <Placeholder fluid style={{ height: '15rem' }} />
                    </Grid.Column>
                    <Grid.Column width="10">
                    <UserInfo user={this.props.user}/>
                    </Grid.Column>
                </Grid.Row>
                </Grid>
                <Grid>
                    <SubMenu />
                    <Posts posts={this.state.posts} />
                </Grid>
            </div>
        )
    }
}

export default User
