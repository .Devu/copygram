import React, { Component } from 'react'
import { Grid, Segment, Placeholder } from 'semantic-ui-react'
import { Link } from 'react-router-dom'

export class Posts extends Component {
    constructor(props) {
        super(props)
        this.state = {
            posts: this.props.posts
        }
    }

    // find(id) {
    //     return this.props.posts.find(p => p.id === id)
    // }

    draw = (posts) => {
        if (posts) {
            return posts.map((post, i) => {
                return (<Grid.Column key={i}>
                    <Segment>
                        <Link to={`/posts/${post.id}`}>
                        <Placeholder fluid style={{ height: '10rem' }} >

                        </Placeholder>
                        <Segment.Inline>
                            {/* {post.text} */}
                        </Segment.Inline>
                        
                        </Link>
                    </Segment>
                    </Grid.Column>)
            })
            
        } else return (<div>No data</div>)
    }

    render() {
        return (
            <Grid.Row centered columns="3">
                {this.draw(this.props.posts)}
            </Grid.Row>
        )
    }
}

export default Posts
