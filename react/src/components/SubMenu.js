import React, { Component } from 'react'
import { Menu, Button } from 'semantic-ui-react'
import axios from 'axios'
export class SubMenu extends Component {


    additem = () => {
        console.log("clicked?")
        let url = "http://localhost:3003/posts"
        let id = Math.floor(Math.random() * 1000); 
        let text = Math.random().toString(36).substring(7) + " " + Math.random().toString(36).substring(7)
        let data = { "id": id, "text": text }
        axios.post(url, data).then(response => {
            console.log(response)
        })
    }

    render() {
        return (
            <Menu pointing secondary fluid widths={6}>
                <Menu.Item>
                    Posts
                </Menu.Item>
                <Menu.Item>
                    Some Nonsense
                </Menu.Item>
                <Menu.Item >
                    <Button onClick={this.additem}>
                        Test Add
                    </Button>
                </Menu.Item>
            </Menu>
        )
    }
}

export default SubMenu
