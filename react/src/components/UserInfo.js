import React, { Component } from 'react'
import { Grid } from 'semantic-ui-react'

export class UserInfo extends Component {
    render() {
        let rPosts = Math.floor(Math.random() * 1000); 
        let rFollowers = Math.floor(Math.random() * 1000); 
        let rFollowing = Math.floor(Math.random() * 1000); 

        return (
            <Grid>
           
                <Grid.Row centered columns="2">
                    <Grid.Column width="10">
                        {this.props.user}
                    </Grid.Column>
                    <Grid.Column width="4">
                        Edit Profile?
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row centered columns="3">
                <Grid.Column>
                      {rPosts}  Posts
                    </Grid.Column>
                    <Grid.Column>
                      {rFollowers}  Followers
                    </Grid.Column>
                    <Grid.Column>
                       {rFollowing} Following
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in varius nisl, vitae finibus erat. In odio dui, ultricies ac pellentesque ac, mattis id ex. Proin sagittis felis in justo condimentum, eu auctor urna consequat. Sed a luctus nulla. Nullam ut suscipit mi, sit amet vestibulum urna. Phasellus fringilla magna et sollicitudin egestas. Aenean consequat ultricies odio feugiat lacinia
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }
}

export default UserInfo
