import React, { Component } from 'react'
import { Grid, Placeholder, Comment, Header, Form, Button } from 'semantic-ui-react'
import axios from 'axios'

export class Post extends Component {

    constructor(props) {
        super(props)
        this.state = {
            comments: {}
        }
    }

    componentDidMount() {
        let url = "http://localhost:3003/"
    
        axios.get(url + "comments").then((res) => {
          // console.log(res.data)
          this.setState({ comments: res.data})
        })
    }


    comment = (number) => {
        for (let i = 0; i < number; i++) {
            let name = Math.random().toString(36).substring(7)
            let text = Math.random().toString(36).substring(7) + " " + Math.random().toString(36).substring(7);
            // this.setState({
            //     comments: [...this.state.comments, {"name": name, "text": text}]
            // })
            this.state.comments.push({ "name" : name, "text": text})
        }
    }

    makeComments = (comments) => {
        // console.log(comments)
        if (comments.length > 1) {
            return comments.map((comment, i) => {
                return (
                    <Comment key={i}>
                    <Comment.Avatar src='https://react.semantic-ui.com/images/avatar/small/matt.jpg' />
                    <Comment.Content>
                      <Comment.Author as='a'>{comment.name}</Comment.Author>
                      <Comment.Metadata>
                        <div>Today at 5:42PM</div>
                      </Comment.Metadata>
                      <Comment.Text>{comment.text}</Comment.Text>
                      <Comment.Actions>
                        <Comment.Action>Reply</Comment.Action>
                      </Comment.Actions>
                    </Comment.Content>
                  </Comment>)
            })
        } else {
            return (
                <div>
                    no comments...
                </div>
            )
        }
    }

    render() {
        return (
            <div style={{ margin: 'auto', paddingTop: '2rem'}}>
                <Grid centered columns="2" celled>
                    <Grid.Row>
                        <Grid.Column width="10">
                            {/* id: {this.props.match.params.id} */}
                            <Placeholder inverted fluid style={{ height: '30rem' }} />
                        </Grid.Column>
                        <Grid.Column width="6">

                            {/* {this.comment(commentNumber)} */}
                            <Header as='h3' dividing>
                            Comments:
                            </Header>
                            <Comment.Group>
                                {this.makeComments(this.state.comments)} 
                            </Comment.Group>
                           
                              <Form reply>
                                <Form.TextArea />
                                <Button content='add reply'/>
                              </Form>
                        
                        </Grid.Column>
                    </Grid.Row>

                </Grid>
            </div>
        )
    }
}

export default Post
