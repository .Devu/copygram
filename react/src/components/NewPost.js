import React, { Component } from 'react'
import { Grid, Placeholder, Button, Form } from 'semantic-ui-react'
import { v4 as uuidv4 } from 'uuid';

export class NewPost extends Component {

    constructor(props) {
        super(props)
        this.state = {
            text: '',
            submittedText: '',
        }
    }

	onChange = (event) => {
		let state = {};
		state[event.target.name] = event.target.value;
		this.setState(state);
    }
    
    onSubmit = (event) => {
		event.preventDefault();
        console.log(this.state.text)

        let id = uuidv4()

        let url = "http://localhost:3003/api/post"
        let item = {
            "text": this.state.text,
            "id": id
        }
        console.log(id)
      
        let request = {
			method:"POST",
			mode:"cors",
            headers:{"Content-type":"application/json", token: this.props.token},
            body:JSON.stringify(item)
        }

        fetch(url, request).then(response => {
            if(response.ok) {
                console.log("success?")
            } else {
                console.log("error")
            }
        }).catch(error => {
            console.log("more errors")
        })

		
	}

    render() {
        return (
            <div >
                <Grid celled>
                <br />
                <Grid.Row centered columns="1">
                    <Grid.Column>
                        <Placeholder fluid style={{ height: '15rem' }} />
                        <br />
                            <div style={{ textAlign: 'center'}}>
                                <Button content='Add Picture'></Button>
                                <br />
                                <br />

                            <Form>
                                <Form.Field style={{Height:"10rem"}}> 
                                <label htmlFor="text">Add Post text:</label>
						        <input type="text"	
								name="text"
								onChange={this.onChange}
								value={this.state.text}/>
                                </Form.Field>
                                <br />
                                <br />
                                <Button onClick={this.onSubmit} name="register">Post</Button>
                            </Form>
                            </div>

                    </Grid.Column>
                </Grid.Row>
                </Grid>
                <Grid>
                    {this.state.submittedText}
             
                 
                </Grid>
            </div>
        )
    }
}

export default NewPost
